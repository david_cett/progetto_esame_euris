﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProgettoCinema.Web.Models
{
    public class VenditaView
    {
       
        public string Nome { get; set; }
        public string Cognome { get; set; }

        public DateTime DataDiNascita { get; set; }  
        
        public string Posto { get; set; }

        public double Prezzo { get; set; }

        public Spettatore GetSpettatore()
        {
            return new Spettatore()
            {
                Nome = this.Nome,
                Cognome = this.Cognome,
                DataDiNascita = Convert.ToString(this.DataDiNascita)
            };
        }

        public Biglietto GetBiglietto()
        {
            return new Biglietto()
            {
                Posto=this.Posto,
                Prezzo=this.Prezzo

            };
        }
    }
}