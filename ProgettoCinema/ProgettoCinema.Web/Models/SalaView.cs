﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProgettoCinema.Web.Models
{
    public class SalaView
    {
        public int IdSala { get; set; }
        public string NomeFilm { get; set; }

        public string Genere { get; set; }

        public int Durata { get; set; }

        public double Incasso { get; set; }

    }
}