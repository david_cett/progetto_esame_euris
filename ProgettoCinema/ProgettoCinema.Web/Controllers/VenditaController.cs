﻿using ProgettoCinema.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgettoCinema.Web.Controllers
{
    public class VenditaController : Controller
    {
        private readonly SpettatoreSqlProvider _spettatoreSqlProvider;
        private readonly BigliettoSqlProvider _bigliettoSqlProvider;
        private readonly SalaSqlProvider _salaSqlProvider;

        public VenditaController()
        {
            _spettatoreSqlProvider = new SpettatoreSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _bigliettoSqlProvider = new BigliettoSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _salaSqlProvider = new SalaSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
        }

       
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(VenditaView vendita)
        {
            
            var biglietti = _bigliettoSqlProvider.GetAll();
            var sala = _salaSqlProvider.Get();

            

            if (biglietti.Count()<=sala.Capienza)
            {
                int posto;
                
                if (biglietti == null)
                {
                    posto = 1;

                }
                else
                {
                    var postoMax = biglietti.Max(n => n.Posto);
                    posto = Convert.ToInt32(postoMax) + 1;

                }

                var biglietto = new Biglietto();
                biglietto.Posto = posto.ToString();

                var spett = vendita.GetSpettatore();
                biglietto.Prezzo = biglietto.GetPrezzoScontato(spett);
                _bigliettoSqlProvider.Insert(biglietto);

                var biglietti2 = _bigliettoSqlProvider.GetAll();
                var id = biglietti2.Max(n => n.IdBiglietto);
                spett.IdBiglietto = id.ToString();
                _spettatoreSqlProvider.Insert(spett);
                return RedirectToAction("Index", "Sala");
            }
            else
            {
                return RedirectToAction("SalaPiena");
            }
           
            
        }

        public ActionResult SalaPiena()
        {
            return View();
        }

        

       

       
    }
}