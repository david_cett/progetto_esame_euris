﻿using ProgettoCinema.Web.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProgettoCinema.Web.Controllers
{
    public class SalaController : Controller
    {
        private readonly SalaSqlProvider _salaSqlProvider;
        private readonly FilmSqlProvider _filmSqlProvider;
        private readonly BigliettoSqlProvider _bigliettoSqlProvider;
        private readonly SpettatoreSqlProvider _spettatoreSqlProvider;

        public SalaController()
        {
            _salaSqlProvider = new SalaSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _filmSqlProvider = new FilmSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _bigliettoSqlProvider = new BigliettoSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
            _spettatoreSqlProvider = new SpettatoreSqlProvider(ConfigurationManager.ConnectionStrings["connectionLibrary"].ConnectionString);
        }
        public ActionResult Index()
        {
            var sala = _salaSqlProvider.Get();
            var film = _filmSqlProvider.GetFilter(sala.IdFilmProiettato);

            var salaView = new SalaView()
            {
                IdSala = sala.IdSala,
                NomeFilm = film.Titolo,
                Genere=film.Genere,
                Durata=film.Durata


            };
            return View(salaView);
        }

        public ActionResult SvuotaSala()
        {
            _bigliettoSqlProvider.DeleteAll();
            _spettatoreSqlProvider.DeleteAll();
            return RedirectToAction("Index", "Sala");
        }
        [Route("Sala/GetIncasso/{id}")]
        public ActionResult GetIncasso(string id)
        {
            var biglietti=_bigliettoSqlProvider.GetAll();
            double incasso = 0;
            foreach(var b in biglietti)
            {
                incasso = incasso + b.Prezzo;
            }
            var sala = new SalaView()
            {
                Incasso = incasso,
                IdSala = Convert.ToInt32(id)
            };
            return View(sala);
        }
    }
}