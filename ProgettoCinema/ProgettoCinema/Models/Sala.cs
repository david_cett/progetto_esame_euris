﻿using System.Collections.Generic;

namespace ProgettoCinema
{
    public class Sala
    {
        public int IdSala { get; set; }
        public int Capienza { get; set; }

        public List<Spettatore> Spettatori { get; set; }
        public int IdFilmProiettato { get; set; }
        public Film FilmProiettato { get; set; }

        public double IncassoTot { get; set; }

        

       

    }
}
