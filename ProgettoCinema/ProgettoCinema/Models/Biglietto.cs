﻿using System;

namespace ProgettoCinema
{
    public class Biglietto
    {
        public int IdBiglietto { get; set; }
        public string Posto { get; set; }

        public const Double PrezzoIntero = 10;
        public double Prezzo { get; set; }

        public bool GetMagiorenne(Spettatore spettatore)
        {
            var now = DateTime.Today;
            var age = now.Year - Convert.ToDateTime(spettatore.DataDiNascita).Year;
            if (now < Convert.ToDateTime(spettatore.DataDiNascita).AddYears(age)) age--;

            if (age < 18)
                return false;

            return true;
        }

        public double GetPrezzoScontato(Spettatore spettatore)
        {
            var now = DateTime.Today;
            var age = now.Year - Convert.ToDateTime(spettatore.DataDiNascita).Year;
            if (now < Convert.ToDateTime(spettatore.DataDiNascita).AddYears(age)) age--;

            if (age < 5)
            {
                return PrezzoIntero - (PrezzoIntero * 0.5);
            }
            else if (age >= 70)
            {
                return PrezzoIntero - (PrezzoIntero * 0.1);
            }
            return PrezzoIntero;
            

        }

    }
}
