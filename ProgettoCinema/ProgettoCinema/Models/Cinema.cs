﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCinema
{
    public class Cinema
    {
        public List<Sala> Sale { get; set; }

        public double GetIncasso()
        {
            double Incasso = 0;
            foreach(var s in Sale)
            {
                Incasso = Incasso + s.IncassoTot;
            }
            return Incasso;
        }
    }
}
