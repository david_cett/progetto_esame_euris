﻿namespace ProgettoCinema
{
    public class Spettatore
    {
        public string IdSpettatore { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }

        public string DataDiNascita { get; set; }

        public string IdBiglietto { get; set; }
        public Biglietto BigliettoAcquistato { get; set; }

    }
}
