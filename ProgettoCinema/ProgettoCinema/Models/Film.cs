﻿namespace ProgettoCinema
{
    public class Film
    {
        public int IdFilm { get; set; }
        public string Titolo { get; set; }
        public string Autore { get; set; }
        public string Produttore { get; set; }
        public string Genere { get; set; }
        public int Durata { get; set; }
    }
}
