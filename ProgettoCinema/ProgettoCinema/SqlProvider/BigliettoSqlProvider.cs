﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCinema
{
    public class BigliettoSqlProvider:SqlProvider,ISqlProvider<Biglietto>
    {

        public BigliettoSqlProvider(string connectionString):base(connectionString)
        {

        }

        public void Insert(Biglietto biglietto)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"INSERT INTO [dbo].[Biglietti]
                                                       ([Posto]
                                                       ,[Prezzo])
                                                 VALUES
                                                       (@Posto
                                                       ,@Prezzo)
                                            ", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@Posto", biglietto.Posto);
                cmd.Parameters.AddWithValue("@Prezzo", biglietto.Prezzo.ToString());
               
                cmd.ExecuteNonQuery();
            }
        }

        public IEnumerable<Biglietto> GetAll()
        {
            List<Biglietto> biglietti = new List<Biglietto>();

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdBiglietto]
                                                  ,[Posto]
                                                  ,[Prezzo]
                                              FROM [dbo].[Biglietti]
                                            ", conn))
            {                                            
                conn.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    biglietti.Add(new Biglietto()
                    {
                        Posto = reader["Posto"].ToString(),
                        Prezzo = Convert.ToDouble(reader["Prezzo"]),
                        IdBiglietto = Convert.ToInt32(reader["IdBiglietto"])


                    });
            }
            return biglietti;
        }

        public void DeleteAll()
        {
            var sqlQuery = @"DELETE FROM [dbo].[Biglietti]";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                using (var command = new SqlCommand(sqlQuery, sqlConnection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
