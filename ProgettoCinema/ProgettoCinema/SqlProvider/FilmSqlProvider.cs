﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCinema
{
    public class FilmSqlProvider:SqlProvider,IGetFilterSqlProvider<Film>
    {
        public FilmSqlProvider(string connectionString):base(connectionString)
        {

        }
        public Film GetFilter(int idFilm)
        {


            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdFilm]
                                              ,[Titolo]
                                             
                                              
                                              ,[Genere]
                                              ,[Durata]
                                          FROM [dbo].[Films]
                                           WHERE IdFilm=@IdFilm", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@IdFilm", idFilm);
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new Film()
                    {
                        Titolo = Convert.ToString(reader["Titolo"]),
                        Genere = Convert.ToString(reader["Genere"]),
                        Durata=Convert.ToInt32(reader["Durata"])
                    };
                }

            }
            return null;
        }
    }
}
