﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCinema
{
    public class SpettatoreSqlProvider:SqlProvider,ISqlProvider<Spettatore>
    {

        public SpettatoreSqlProvider(string connectionString):base(connectionString)
        {

        }
        public void Insert(Spettatore spettatore)
        {
            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"INSERT INTO [dbo].[Spettatori]
                                               ([Nome]
                                               ,[Cognome]
                                               ,[DataDiNascita]
                                               ,[IdBiglietto])
                                         VALUES
                                               (@Nome
                                               ,@Cognome
                                               ,@DataDiNascita
                                               ,@IdBiglietto)", conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@Nome", spettatore.Nome);
                cmd.Parameters.AddWithValue("@Cognome", spettatore.Cognome);
                cmd.Parameters.AddWithValue("@DataDiNascita",spettatore.DataDiNascita);
                cmd.Parameters.AddWithValue("@IdBiglietto", spettatore.IdBiglietto);              
                cmd.ExecuteNonQuery();
            }
        }

        public IEnumerable<Spettatore> GetAll()
        {
            List<Spettatore> spettatori = new List<Spettatore>();

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdSpettatore]
                                              ,[Nome]
                                              ,[Cognome]
                                              ,[DataDiNascita]
                                              ,[IdBiglietto]
                                          FROM [dbo].[Spettatori]", conn))
            {
                conn.Open();
                var reader = cmd.ExecuteReader();
                while (reader.Read())
                    spettatori.Add(new Spettatore()
                    {
                        Nome = reader["Nome"].ToString(),
                        Cognome = reader["Cognome"].ToString(),
                        DataDiNascita = reader["DataDiNascita"].ToString(),
                        IdBiglietto = reader["IdBiglietto"].ToString()
                        
                    });
            }
            return spettatori;
        }
        public void DeleteAll()
        {
            var sqlQuery = @"DELETE FROM [dbo].[Spettatori]";

            using (var sqlConnection = new SqlConnection(connectionString))
            {
                sqlConnection.Open();

                using (var command = new SqlCommand(sqlQuery, sqlConnection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
