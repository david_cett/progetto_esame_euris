﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCinema
{
    public class SalaSqlProvider:SqlProvider,IGetSqlProvider<Sala>
    {
        public SalaSqlProvider(string connectionString):base(connectionString)
        {

        }

        public Sala Get()
        {
            

            using (var conn = new SqlConnection(connectionString))
            using (var cmd = new SqlCommand(@"SELECT [IdSala]
                                                  ,[Capienza]
                                                  ,[IdFilm]
                                                  ,[IncassoTot]
                                              FROM [dbo].[Sale]", conn))
            {
                conn.Open();
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new Sala()
                    {
                        IdSala = Convert.ToInt32(reader["IdSala"]),
                        IdFilmProiettato = Convert.ToInt32(reader["IdFilm"]),
                        Capienza = Convert.ToInt32(reader["Capienza"]),
                    };
                }
                   
            }
            return null;
        }

        
    }
}
