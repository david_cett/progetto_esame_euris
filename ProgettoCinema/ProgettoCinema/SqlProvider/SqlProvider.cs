﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCinema
{
    public abstract class SqlProvider
    {
        protected readonly string connectionString;

        protected SqlProvider(string connectionString)
        {
            this.connectionString = connectionString;
        }


    }
}
