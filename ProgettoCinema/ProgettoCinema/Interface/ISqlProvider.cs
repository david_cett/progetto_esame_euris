﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCinema
{
    interface ISqlProvider<T>
    {
        void Insert(T entity);
        IEnumerable<T> GetAll();
        void DeleteAll();
    }
}
