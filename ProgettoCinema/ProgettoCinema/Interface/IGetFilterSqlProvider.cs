﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgettoCinema
{
    interface IGetFilterSqlProvider<T>
    {
        T GetFilter(int id);
    }
}
